# Terraform - Configure a Shared Remote State 

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Configure Amazon S3 as remote storage for Terraform 

## Technologies Used 

* Terraform 

* AWS 

* S3 

## Steps 

Step 1: Create S3 bucket on AWS that will store terraform state 

[Creating s3 bucket storage](/images/01_create_s3_bucket_that_will_store_terraform_state.png)

[Created bucket](/images/02_created_bucket.png)

Step 2: Add S3 remote storage in main file in the terraform block 

[Adding s3 remote storage in main file](/images/03_add_s3_remote_storage_in_main_file_in_terraform_block.png)

Step 3: In the provisioning server block in Jenkinfile add -force-copy flag to the terraform init, this will prevent any prompts from interrupting Jenkins building the Pipeline 

[Force copy flag](/images/04_in_the_provision_server_block_in_Jenkinsfile__in_the_script_section_put_terraform_init_force_copy_to_prevent_any_prompts_from_interrupting_jenkins.png)

Step 4: Push change to application remote repo 

[Push Changes to repo](/images/05_push_changes_to_application_remote_repo.png)

Step 5: Build Pipeline 

[Successful Pipeline](/images/06_successful_pipeline.png)

Step 6: Check if state got stored in s3 bucket on AWS console 

[Bucket Statfile](/images/s3_bucket_statfile_uploaded.png)

## Installation 

    brew install terraform 

## Usage 

    Terraform apply 

## How to Contribute

1. Clone the repo using $ git clone https://gitlab.com/omacodes98/terraform-configure-a-shared-remote-state.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

Projects included 

https://gitlab.com/omacodes98/terraform-configure-a-shared-remote-state

https://gitlab.com/omacodes98/terraform-automate-aws-infrastructure.git

https://gitlab.com/omacodes98/complete-ci-cd-with-terraform.git

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.